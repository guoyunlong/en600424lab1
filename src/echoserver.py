from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory
import time

from playground.twisted.endpoints import GateServerEndpoint


class EchoServer(Protocol):

    def __init__(self):
        self.root = '/home/king1995128/PycharmProjects/networkSecurityLab1'

    def dataReceived(self, data):
        self.transport.write(self.extract_document_name(data))

    def extract_document_name(self, request):
        req_list = request.split()

        if req_list[0] == 'GET' and (req_list[2] == 'HTTP/1.0' or req_list[2] == 'HTTP/1.1'):
            try:
                message_result_line = 'HTTP/1.0 200 OK\n'
                date = time.ctime()
                response_header = 'Date:' + date + '\nContent-Type: text/html\nServer: echoServer2.0\n' \
                                                   'Connection: Closed\n'
                f = open(self.root + req_list[1])
                data = f.read()
                result = message_result_line + response_header + data


            except:
                message_result_line = 'HTTP/1.0 404 NOT FOUND\n'
                date  = time.ctime()
                response_header = 'Date:' + date + '\nContent-Type: text/html\nServer: echoServer2.0\n' \
                                                   'Connection: Closed\n'
                f = open(self.root + "/file/index.html")
                data = f.read()
                result = message_result_line + response_header + data
            # return req_list[1]
            f.close()
            return result

        else:
            return 'syntax is wrong'


def main():
    factory = ServerFactory()
    factory.protocol = EchoServer
    endpoint = GateServerEndpoint.CreateFromConfig(reactor, 101)
    endpoint.listen(factory)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
