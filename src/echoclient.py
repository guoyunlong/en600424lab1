from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet import reactor

from playground.twisted.endpoints import GateClientEndpoint


class EchoClient(Protocol):

    def __init__(self):
        pass

    def connectionMade(self):
        msg = raw_input('Please input the resource and the path you request')
        self.transport.write(self.wrapmsg(msg))

    def dataReceived(self, data):
        print data

    def wrapmsg(self, msg):
        msg = 'GET ' + msg + ' HTTP/1.0\r\n Host: <gatekey>\r\n\r\n'
        return msg


class EchoClientFactory(ClientFactory):

    def __init__(self):
        pass

    def startedConnecting(self, connector):
        print 'started connecting'

    def buildProtocol(self, addr):
        print 'connected'
        return EchoClient()

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection. Reason:', reason
        print 'prepare to reconnect'
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason', reason


class Helper:

    def __init__(self):
        pass


def main():

    f = EchoClientFactory()
    endpoint = GateClientEndpoint.CreateFromConfig(reactor, '20164.0.0.1', 101)
    endpoint.connect(f)
    reactor.run()


if __name__ == '__main__':
    main()





